var greetings_data,rev_greetings_duration;

$(".datatable").DataTable();
$(document).on('change', '.rev_greetings_filter', function () 
    {
        var filter_val=$(this).val();
        if(filter_val=="today")
        {
            rev_greetings_duration=["Today"];
            greetings_data=[5];
            
        }
        else if(filter_val=="yesterday")
        {
            rev_greetings_duration=["Yesterday"];
            greetings_data=[3];
            
        }
        else if(filter_val=="this_week")
        {
            rev_greetings_duration=["Mon", "Tue", "Wed","Thr","Fri","Sat","Sun"];
            greetings_data=[3,6,8,3,9,4,1];
            
        }
        else if(filter_val=="last_week")
        {
            rev_greetings_duration=["Mon", "Tue", "Wed","Thr","Fri","Sat","Sun"];
            greetings_data=[4,2,4,3,9,4,1];
            
        }
        else if(filter_val=="last_6_months")
        {
            rev_greetings_duration = ["Mon", "Tue", "Wed","Thr","Fri","Sat","Sun"];
            greetings_data=[14,12,24,13,29,40];
            
        }
        else if(filter_val=="last_year")
        {
            rev_greetings_duration = ["Jan", "Feb", "Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
            greetings_data=[14,12,24,13,29,40,34,5,78,12,23,12];
            
        }
        rev_greetings_chart();
    });
    rev_greetings_duration = ["Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
    greetings_data=[40,22,34,13,25,40];
   
    rev_greetings_chart();



    


    
    

    function rev_greetings_chart()
    {
        Highcharts.chart('rev_greetings_chart', {
            chart: {
                type: 'areaspline'
            },
            colors: ['#FF7D43'],
            title: {
                text: ''
            },
            legend: {
                layout: 'vertical',
                align: 'left',
                verticalAlign: 'top',
                x: 150,
                y: 100,
                floating: true,
                borderWidth: 1,
                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
            },
            xAxis: {
                categories: rev_greetings_duration,
            
            },
            yAxis: {
                title: {
                    text: '₹'
                }
            },
            tooltip: {
                shared: true,
                valueSuffix: ' M'
            },
            credits: {
                enabled: false
            },
            plotOptions: {
                areaspline: {
                    fillOpacity: 0.5
                },
                
            },
            series: [{
                name: 'Greetings',
                data: greetings_data
            }]
        });
    }
    
    

    