$(".datatable").DataTable();
$(document).on('click', '#add_roles', function () 
{
    $("#add_roles_modal .modal-title").html("Add Roles");
    $('#add_roles_modal').modal('show');
});
$(document).on('click', '.activate_roles', function () 
{
    var id=$(this).closest('tr').attr("id");
    var row_id = id.split("_");
    var data_id=row_id[1];
    alert(data_id);
});
$(document).on('click', '.deactivate_roles', function () 
{
    var id=$(this).closest('tr').attr("id");
    var row_id = id.split("_");
    var data_id=row_id[1];
    alert(data_id);
});


$(document).on('click', '.edit_roles', function () 
{
    var id=$(this).closest('tr').attr("id");
    var roles=$("tr#"+id+" td.roles").html();
    if(roles=="Dispute Manager")
    {
        roles="dispute_manager";
    }
    else if(roles=="Celebrity Manager")
    {
        roles="celebrity_manager";
    }
    else if(roles=="Review Manager")
    {
        roles="review_manager";
    }
    else if(roles=="Super Admin")
    {
        roles="super_admin";
    }
    $("#user_name").val($("tr#"+id+" td.user_name").html());
    $("#email_id").val($("tr#"+id+" td.email").html());
    $("#mobile_no").val($("tr#"+id+" td.mobile_no").html());
    $("#roles").val(roles);
    $("#add_roles_modal .modal-title").html("Edit Roles");
    $('#add_roles_modal').modal('show');
});