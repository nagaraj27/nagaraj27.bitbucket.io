var playerSettings = {
    controls : ['play-large', 'progress', 'current-time', 'rewind', 'restart', 'play'],
    fullscreen : { enabled: false},
    resetOnEnd : true,
    hideControls  :false,
    keyboard : false
}
 $(document).on('click', '.table-action', function () 
 {
    var clickedElement = $(this).attr("id");
    
    if(clickedElement=="close_dispute")
    {
        $('#close_dispute_modal').modal('show');
    }
    else if(clickedElement=="video_detail" || clickedElement=="calendar_detail" || clickedElement=="full_detail" || clickedElement=="dispute_detail")
    {
        $('#video_detail_modal').modal('show');
        
        $('#video_detail_modal #disp_comp_id').html($(this).data("compid"));
        $('#video_detail_modal #disp_queued_on').html($(this).data("queuedon"));
        $('#video_detail_modal #disp_video_call_to').html($(this).data("video-call-to"));
        $('#video_detail_modal #disp_credit_spent').html($(this).data("credit-spent"));
        $('#video_detail_modal #disp_expec_dt_call').html($(this).data("exp-dt-of-call"));
        $('#video_detail_modal #disp_exact_dt_time').html($(this).data("exct-dt-time"));
        $('#video_detail_modal #disp_raised_on').html($(this).data("disp-raised-on"));
        $('#video_detail_modal #disp_raised_by').html($(this).data("disp-raised-by"));
        $('#video_detail_modal #disp_against').html($(this).data("dispute-against"));
        $('#video_detail_modal #disp_on').html($(this).data("dispute-on"));
        $('#video_detail_modal #disp_raised_option').html($(this).data("dispute-raised-option"));
        $('#video_detail_modal #disp_raised_comment').html($(this).data("dispute-raised-comment"));
        $('#video_detail_modal #disp_closed_on').html($(this).data("dispute-closed-on"));
        $('#video_detail_modal #disp_closed_option').html($(this).data("dispute-closed-option"));
        $('#video_detail_modal #disp_closed_comment').html($(this).data("dispute-closed-comment"));
        
        $('#video_detail_modal #disp_second_appeal').html($(this).data("second-appeal"));
        $('#video_detail_modal #disp_second_appeal_option').html($(this).data("second-appeal-option"));
        $('#video_detail_modal #disp_second_appeal_comment').html($(this).data("second-appeal-comment"));
        if(clickedElement=="video_detail")
        {   
            $(".full_detail_div").hide();
            $(".calendar_div").hide();
             $('#modal_div').addClass("modal-lg");
            $("#column_div").removeClass("col-md-12").addClass("col-md-6");
            $(".video_div").show();
            const player = new Plyr('#dispute-video', playerSettings);
            player.source = {
                type: 'video',
                title: 'Example title',
                sources: [
                    {
                        src: $(this).data("video"),
                        type: 'video/mp4',
                        size: 576,
                    }
                ],
                poster: $(this).data("video-poster")
            }
            player.on('ended',function(){
                $('.video-review-approve').removeAttr('disabled');
                $('.video-review-reject').removeAttr('disabled');
            });
        }
        else if(clickedElement=="calendar_detail")
        {
            $("#calendar").html(" ").html('<div id="inline_calendar" ></div>');
            $(".full_detail_div").hide();
            $('#modal_div').addClass("modal-lg");
            $("#column_div").removeClass("col-md-12").addClass("col-md-6");
            $(".video_div").hide();
            $('#video_detail_modal #celebrity_status').html($(this).data("celebrity-status"));
            $('#video_detail_modal #expec_date_of_call').html($(this).data("expected-date-of-call"));
            $('#video_detail_modal #expec_time_of_call').html($(this).data("expected-time-of-call"));
            
            var available_dates=$(this).data("available-dates");
            var element = document.getElementById("inline_calendar");
            var myCalendar = jsCalendar.new(element);
            myCalendar.reset();
            myCalendar.select(available_dates);
            $(".calendar_div").show();
        }
        else if(clickedElement=="full_detail")
        {
            $(".full_detail_div").show();
            $('#modal_div').removeClass("modal-lg");
            $(".second_appeal").show();
            $(".video_div").hide();
            $(".calendar_div").hide();
            $("#column_div").removeClass("col-md-6").addClass("col-md-12");
        }
        else if(clickedElement=="dispute_detail")
        {
            $(".full_detail_div").show();
            $(".second_appeal").hide();
            $('#modal_div').removeClass("modal-lg");
            $(".video_div").hide();
            $(".calendar_div").hide();
            $("#column_div").removeClass("col-md-6").addClass("col-md-12");
        }
        
    }
});
$(document).on('click', '.close_dispute', function () 
{
     $('#close_dispute_modal').modal('show');
});