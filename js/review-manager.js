
var playerSettings = {
      controls : ['play-large', 'progress', 'current-time', 'rewind', 'restart', 'play'],
      fullscreen : { enabled: false},
      resetOnEnd : true,
      hideControls  :false,
      keyboard : false
    }

//Click events for action buttons in table

  $(document).on('click', '.table-action', function () {
    var clickedElement = $(this);
    $('.video-review-approve').prop('disabled', true);
    $('.video-review-reject').prop('disabled', true);
    $('.video-review-approve, .video-review-reject').attr('data-reviewId', clickedElement.attr('data-reviewId'))
    $('.rev-det').html("");

    // Personal Autograph / Greetings

    if (clickedElement.hasClass('rev-personalAutograph')) {
        $('#rev-personalAutograph').modal('show');
        $('#rev-personalAutograph .rev-fan-id').html($(this).attr("data-fanId"));
        $('#rev-personalAutograph .rev-fan-name').html($(this).attr("data-fanName"));
        $('#rev-personalAutograph .rev-category').html($(this).attr("data-category"));
        $('#rev-personalAutograph .rev-date').html($(this).attr("data-date"));
        $('#rev-personalAutograph .rev-celeb-name').html($(this).attr("data-celebName"));
        $('#rev-personalAutograph .rev-uploaded-img').html($(this).attr("data-filename"));
        $('#rev-personalAutograph .rev-imag-raw img').attr("src", $(this).attr("data-fileSrc"));
        $('#rev-personalAutograph .rev-message div').html($(this).attr("data-message"));
        $('#rev-personalAutograph .rev-img-composite img').attr("src", $(this).attr("data-fileSrc-composite"));
    }

    // Exclusive Video

    else if (clickedElement.hasClass('rev-exclusiveVideo')) {
        $('#rev-exclusiveVideo').modal('show');
        $('#rev-exclusiveVideo .rev-celeb-id').html($(this).attr("data-celebId"));
        $('#rev-exclusiveVideo .rev-celeb-name').html($(this).attr("data-celebName"));
        $('#rev-exclusiveVideo .rev-category').html($(this).attr("data-category"));
        $('#rev-exclusiveVideo .rev-duration').html($(this).attr("data-duration"));
        $('#rev-exclusiveVideo .rev-date').html($(this).attr("data-date"));
        $('#rev-exclusiveVideo .rev-uploaded-by').html($(this).attr("data-uploaded-by"));
        $('#rev-exclusiveVideo .rev-video').attr("src", $(this).attr("data-video"));
        const player = new Plyr('#rev-exclusive-video', playerSettings);
        player.source = {
            type: 'video',
            title: 'Example title',
            sources: [
                {
                    src: $(this).attr("data-video"),
                    type: 'video/mp4',
                    size: 576,
                }
            ],
            poster: $(this).attr("data-video-poster")
        }
        player.on('ended',function(){
            $('.video-review-approve').removeAttr('disabled');
            $('.video-review-reject').removeAttr('disabled');
          });
    }
   
   // Exclusive Image

    else if (clickedElement.hasClass('rev-exclusiveImage')) {
        $('#rev-exclusiveImage').modal('show');
        $('#rev-exclusiveImage .rev-celeb-id').html($(this).attr("data-celebId"));
        $('#rev-exclusiveImage .rev-celeb-name').html($(this).attr("data-celebName"));
        $('#rev-exclusiveImage .rev-category').html($(this).attr("data-category"));
        $('#rev-exclusiveImage .rev-duration').html($(this).attr("data-duration"));
        $('#rev-exclusiveImage .rev-date').html($(this).attr("data-date"));
        $('#rev-exclusiveImage .rev-uploaded-by').html($(this).attr("data-uploaded-by"));
        $('#rev-exclusiveImage .rev-img img').attr("src", $(this).attr("data-src"));
    }

    // Video Call

    else if (clickedElement.hasClass('rev-videoCall')) {
        $('#rev-videoCall').modal('show');
        $('#rev-videoCall .rev-celeb-id').html($(this).attr("data-celebId"));
        $('#rev-videoCall .rev-celeb-name').html($(this).attr("data-celebName"));
        $('#rev-videoCall .rev-date').html($(this).attr("data-callDate"));
        $('#rev-videoCall .rev-fan-name').html($(this).attr("data-fanName"));
        $('#rev-videoCall .rev-category').html($(this).attr("data-category"));
        $('#rev-videoCall .rev-call-duration').html($(this).attr("data-duration"));

        const player = new Plyr('#rev-call-video', playerSettings);
        player.source = {
            type: 'video',
            title: 'Example title',
            sources: [
                {
                    src: $(this).attr("data-video"),
                    type: 'video/mp4',
                    size: 576,
                }
            ],
            poster: $(this).attr("data-video-poster")
        }
        player.on('ended',function(){
            $('.video-review-approve').removeAttr('disabled');
            $('.video-review-reject').removeAttr('disabled');
          });

    }

    // Video Message

    else if (clickedElement.hasClass('rev-videoMsg')) {
        $('#rev-videoMsg').modal('show');
        $('#rev-videoMsg .rev-fan-id').html($(this).attr("data-fanId"));
        $('#rev-videoMsg .rev-fan-name').html($(this).attr("data-fanName"));
        $('#rev-videoMsg .rev-category').html($(this).attr("data-category"));
        $('#rev-videoMsg .rev-date').html($(this).attr("data-date"));
        $('#rev-videoMsg .rev-message div').html($(this).attr("data-message"));
    }

    //In the News

    else if (clickedElement.hasClass('rev-inNews')) {
        $('#rev-inNews').modal('show');
        $('#rev-inNews .rev-celeb-id').html($(this).attr("data-celebId"));
        $('#rev-inNews .rev-celeb-name').html($(this).attr("data-celebName"));
        $('#rev-inNews .rev-category').html($(this).attr("data-category"));
        $('#rev-inNews .rev-date').html($(this).attr("data-date"));
        $('#rev-inNews .rev-uploaded-by').html($(this).attr("data-uploaded-by"));
        $('#rev-inNews .rev-img img').attr("src", $(this).attr("data-src"));
        var apiUrl = $(this).attr("data-api-url")

        //Paragraphs are got using ajax and not data since special characters cannot be escaped in HTML. Refer sample-api-1.txt for sample api

        $.ajax({
            url: apiUrl,
            async: false,
            success: function (data){
                $('#rev-inNews .rev-text').html(data)
            }
        });
    }

// Assign Issue ID to reject form hidden input   

$(document).on('click', '.video-review-reject, .review-reject', function () {
    $("#confirmation-reject").modal('show');
    $("#rejectionId").val(clickedElement.attr('data-reviewId'))
});

});


// Multiple modal backgrouund solved

 $(document).on('show.bs.modal', '.modal', function (event) {
    var zIndex = 1040 + (10 * $('.modal:visible').length);
    $(this).css('z-index', zIndex);
    setTimeout(function() {
        $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
    }, 0);
});