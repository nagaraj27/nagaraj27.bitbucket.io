var playerSettings = {
    controls : ['play-large', 'progress', 'current-time', 'rewind', 'restart', 'play'],
    fullscreen : { enabled: false},
    resetOnEnd : true,
    hideControls  :false,
    keyboard : false
}
var earned_data,no_of_video_calls_data,earings_duration;
$(".datatable").DataTable();
$('.cele_count,.counter').counterUp({
    delay: 10,
    time: 1000
});
$(document).on('click', '.preview_video_call', function () 
{
    
    $("#video_call_preview_modal .queued_on_date").html($(this).data("queuedon"));
    $("#video_call_preview_modal .called_on_date").html($(this).data("calledon"));
    $("#video_call_preview_modal .review_date").html($(this).data("review"));
    $("#video_call_preview_modal .approved_date").html($(this).data("approved"));
   
    const player = new Plyr('#preview_video', playerSettings);
    player.source = {
        type: 'video',
        title: 'Example title',
        sources: [
            {
                src: $(this).attr("data-video"),
                type: 'video/mp4',
                size: 576,
            }
        ],
        poster: $(this).attr("data-video-poster")
    }
    player.on('ended',function(){
        $('.video-review-approve').removeAttr('disabled');
        $('.video-review-reject').removeAttr('disabled');
      });
      $('#video_call_preview_modal').modal('show');
});
$(document).on('change', '.earn_video_calls_filter', function () 
    {
        var filter_val=$(this).val();
        if(filter_val=="today")
        {
            earings_duration=["Today"];
            earned_data=[5];
            no_of_video_calls_data=[8];
        }
        else if(filter_val=="yesterday")
        {
            earings_duration=["Yesterday"];
            earned_data=[3];
            no_of_video_calls_data=[9];
        }
        else if(filter_val=="this_week")
        {
            earings_duration=["Mon", "Tue", "Wed","Thr","Fri","Sat","Sun"];
            earned_data=[3,6,8,3,9,4,1];
            no_of_video_calls_data=[2, 5, 9, 3, 3, 15];
        }
        else if(filter_val=="last_week")
        {
            earings_duration=["Mon", "Tue", "Wed","Thr","Fri","Sat","Sun"];
            earned_data=[4,2,4,3,9,4,1];
            no_of_video_calls_data=[20, 15, 19, 39, 4, 5];
        }
        else if(filter_val=="last_6_months")
        {
            earings_duration = ["Mon", "Tue", "Wed","Thr","Fri","Sat","Sun"];
            earned_data=[14,12,24,13,29,40];
            no_of_video_calls_data=[2, 5, 9, 3, 4,7];
        }
        else if(filter_val=="last_year")
        {
            earings_duration = ["Jan", "Feb", "Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
            earned_data=[14,12,24,13,29,40,34,5,78,12,23,12];
            no_of_video_calls_data=[2, 5, 9, 3, 4,7,54,23,12,34,87,65];
        }
        earn_video_calls_chart();
    });
    earings_duration = ["Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
    earned_data=[40,22,34,13,25,40];
    no_of_video_calls_data=[25, 50, 29, 38, 34,74];
    earn_video_calls_chart();
function earn_video_calls_chart()
{
    Highcharts.chart('earn_video_calls_chart', {
        chart: {
            type: 'areaspline'
        },
        colors: ['#A3A0FB','#54D8FF'],
        title: {
            text: ''
        },
        legend: {
            layout: 'vertical',
            align: 'left',
            verticalAlign: 'top',
            x: 150,
            y: 100,
            floating: true,
            borderWidth: 1,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
        },
        xAxis: {
            categories: earings_duration,
           
        },
        yAxis: {
            title: {
                text: ''
            }
        },
        tooltip: {
            shared: true,
            valueSuffix: ' '
        },
        credits: {
            enabled: false
        },
        plotOptions: {
            areaspline: {
                fillOpacity: 0.5
            },
            
        },
        series: [{
            name: 'Amount Earned',
            data: earned_data
        }, {
            name: 'Number of Video Calls',
            data: no_of_video_calls_data
        }]
    });
}