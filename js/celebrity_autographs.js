var earned_data,no_of_autographs_data,earings_duration;
$(".datatable").DataTable();
$(document).on('click', '.preview_autograph', function () 
{
    $("#autograph_preview_modal .autograph_preview_div").css("background",$(this).data("src"));
    $("#autograph_preview_modal .wish_text, #autograph_preview_modal .wish_text_area").html($(this).data("text"));
    $("#autograph_preview_modal .fan_id_text").html("Fan ID-"+$(this).data("fanid"));

    $("#autograph_preview_modal .queued_on_date").html($(this).data("queuedon"));
    $("#autograph_preview_modal .called_on_date").html($(this).data("calledon"));
    $("#autograph_preview_modal .review_date").html($(this).data("review"));
    $("#autograph_preview_modal .approved_date").html($(this).data("approved"));

    $('#autograph_preview_modal').modal('show');
});
$('.cele_count,.counter').counterUp({
    delay: 10,
    time: 1000
});
$(document).on('change', '.earn_autographs_filter', function () 
    {
        var filter_val=$(this).val();
        if(filter_val=="today")
        {
            earings_duration=["Today"];
            earned_data=[5];
            no_of_autographs_data=[8];
        }
        else if(filter_val=="yesterday")
        {
            earings_duration=["Yesterday"];
            earned_data=[3];
            no_of_autographs_data=[9];
        }
        else if(filter_val=="this_week")
        {
            earings_duration=["Mon", "Tue", "Wed","Thr","Fri","Sat","Sun"];
            earned_data=[3,6,8,3,9,4,1];
            no_of_autographs_data=[2, 5, 9, 3, 3, 15];
        }
        else if(filter_val=="last_week")
        {
            earings_duration=["Mon", "Tue", "Wed","Thr","Fri","Sat","Sun"];
            earned_data=[4,2,4,3,9,4,1];
            no_of_autographs_data=[20, 15, 19, 39, 4, 5];
        }
        else if(filter_val=="last_6_months")
        {
            earings_duration = ["Mon", "Tue", "Wed","Thr","Fri","Sat","Sun"];
            earned_data=[14,12,24,13,29,40];
            no_of_autographs_data=[2, 5, 9, 3, 4,7];
        }
        else if(filter_val=="last_year")
        {
            earings_duration = ["Jan", "Feb", "Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
            earned_data=[14,12,24,13,29,40,34,5,78,12,23,12];
            no_of_autographs_data=[2, 5, 9, 3, 4,7,54,23,12,34,87,65];
        }
        earn_autographs_chart();
    });
    earings_duration = ["Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
    earned_data=[40,22,34,13,25,40];
    no_of_autographs_data=[25, 50, 29, 38, 34,74];
    earn_autographs_chart();
function earn_autographs_chart()
{
    Highcharts.chart('earn_autographs_chart', {
        chart: {
            type: 'areaspline'
        },
        colors: ['#A3A0FB','#54D8FF'],
        title: {
            text: ''
        },
        legend: {
            layout: 'vertical',
            align: 'left',
            verticalAlign: 'top',
            x: 150,
            y: 100,
            floating: true,
            borderWidth: 1,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
        },
        xAxis: {
            categories: earings_duration,
           
        },
        yAxis: {
            title: {
                text: ''
            }
        },
        tooltip: {
            shared: true,
            valueSuffix: ' '
        },
        credits: {
            enabled: false
        },
        plotOptions: {
            areaspline: {
                fillOpacity: 0.5
            },
            
        },
        series: [{
            name: 'Amount Earned',
            data: earned_data
        }, {
            name: 'Number of Autographs',
            data: no_of_autographs_data
        }]
    });
}