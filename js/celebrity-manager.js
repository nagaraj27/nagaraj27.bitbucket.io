// Click event for celebrity name
$(document).on('click', '.celeb-name', function () {
    $('#celeb-edit').modal('show');
    $('#celeb-edit .celeb-edit-img').attr("src", $(this).attr("data-imgUrl"));
    $('#celeb-edit .celeb-edit-name').html($(this).attr("data-name"));
    $('#celeb-edit .celeb-edit-id').html($(this).attr("data-celebId"));
    $('#celeb-edit .celeb-edit-date').html($(this).attr("data-joinDate"));
    $('#celeb-edit .celeb-edit-category').html($(this).attr("data-category"));
    $('#celeb-edit .celeb-edit-manager').html($(this).attr("data-ManagerNo"));
    $('#celeb-edit .celeb-edit-base').html($(this).attr("data-basefare"));
    $('#celeb-edit .celeb-edit-status').html($(this).attr("data-status"));
    $('#celeb-edit .celeb-edit-link').attr("href", "/CelebrityManager/PreviewCeleb?celebId=" + $(this).attr("data-celebId"));
});


//action click event scripts
$(document).on('click', '.action', function () {

    if ($(this).hasClass('action-activate')) {
        //ask dhilip for action
    }

    if ($(this).hasClass('action-inactivate')) {
        $('#inactivate-account').modal('show');
        $("#inactivationId").val(clickedElement.attr('data-celebId'));
    }

    if ($(this).hasClass('action-deactivate')) {
        $('#deactivate-account').modal('show');
        $("#deactivationId").val(clickedElement.attr('data-celebId'));
    }

});







//Celebrity-manager-new-celebrity + edit


$(document).ready(function () {
    var next = 0;
    $(".add-news").click(function (e) {
        e.preventDefault();
        var addto = "#news-group" + next;
        var addRemove = "#news-group" + (next);
        next = next + 1;
        var newIn = ' <div class="newsgrouping" id="news-group' + next + '"><textarea name="inthenews-title-' + next + '" id="inthenews-title-' + next + '" class="inthenews-title" placeholder="News Title"></textarea><input type="file" id="inthenews-image-' + next + '" name="inthenews-image-' + next + '" class="inthenews-image" accept="image/png, image/jpeg"><textarea name="inthenews-content-' + next + '" id="inthenews-content-' + next + '" class="inthenews-content"placeholder="News Content"></textarea></div>';
        var newInput = $(newIn);
        var removeBtn = '<button type="button" id="remove' + (next - 1) + '" class="remove-me" >Delete News</button></div></div><div id="news-group">';
        var removeButton = $(removeBtn);
        $(addto).after(newInput);
        $(addRemove).after(removeButton);
        FilePond.create(
            document.querySelector('#inthenews-image-' + next)
        );
        $("#news-group" + next).attr('data-source', $(addto).attr('data-source'));
        $("#count").val(next);

        $('.remove-me').click(function (e) {
            e.preventDefault();
            var fieldNum = this.id.charAt(this.id.length - 1);
            var fieldID = "#news-group" + fieldNum;
            $(this).remove();
            $(fieldID).remove();
        });
    });

});

$(document).ready(function () {
    //group add limit

    //add more fields group
    $(".add-trailer").click(function () {
        var fieldHTML = '<div class="trailer-input-group row">' + $(".fieldGroupCopy").html() + '</div>';
        $('body').find('.trailer-input-group:last').after(fieldHTML);
    });

    //remove fields group
    $("body").on("click", ".remove-trailer", function () {
        if ($(".trailer-input-group").length > 1) {
            $(this).parents(".trailer-input-group").remove();
        } else {
            alert("Atleast one trailer should be present")
        }
    });

    //add more fields group
    $(".add-award").click(function () {
        var fieldHTML = '<div class="award-input-group row">' + $(".fieldGroupCopy1").html() + '</div>';
        $('body').find('.award-input-group:last').after(fieldHTML);
    });

    //remove fields group
    $("body").on("click", ".remove-award", function () {
        if ($(".award-input-group").length > 1) {
            $(this).parents(".award-input-group").remove();
        } else {
            alert("Atleast one award should be present")
        }
    });

    //add more fields group
    $(".add-didYouKnow").click(function () {
        var fieldHTML = '<div class="didYouKnow-input-group row">' + $(".fieldGroupDidYouKnow").html() + '</div>';
        $('body').find('.didYouKnow-input-group:last').after(fieldHTML);
    });

    //remove fields group
    $("body").on("click", ".remove-didYouKnow", function () {
        if ($(".didYouKnow-input-group").length > 1) {
            $(this).parents(".didYouKnow-input-group").remove();
        } else {
            alert("Atleast one did you know fact should be present")
        }
    });

    //add more fields group
    $(".add-trivia").click(function () {
        var fieldHTML = '<div class="trivia-input-group row">' + $(".fieldGroupCopy2").html() + '</div>';
        $('body').find('.trivia-input-group:last').after(fieldHTML);
    });

    $("body").on("click", ".remove-trivia", function () {
        if($(".trivia-input-group").length > 1){
            $(this).parents(".trivia-input-group").remove();
        } else {
            alert("Atleast one did you know fact should be present")
        }
    });

    //add more fields group
    $(".add-news-edit").click(function () {
        var fieldHTML = '<div class="newsgrouping-edit">' + $(".fieldGroupCopy3").html() + '</div>';
        $('body').find('.newsgrouping-edit:last').after(fieldHTML);
    });

    $("body").on("click", ".remove-trivia", function () {
        if($(".trivia-input-group").length > 1){
            $(this).parents(".trivia-input-group").remove();
        } else {
            alert("Atleast one did you know fact should be present")
        }
    });
});

function imageset(event) {
    var input = event.target;
    var preview = document.getElementById("celeb_profilepic");
    var file = input.files[0]; //sames as here
    var reader = new FileReader();
    reader.onloadend = function () {
        preview.src = reader.result;
        document.getElementById("profile_pic_data").value = reader.result;
    };
    if (file) {
        reader.readAsDataURL(file); //reads the data as a URL
    }
}

function newsimageset(event) {
    var input = event.target;
    var preview = document.getElementById("news-pic");
    var file = input.files[0]; //sames as here
    var reader = new FileReader();
    reader.onloadend = function () {
        preview.src = reader.result;
        document.getElementById("news_pic_data").value = reader.result;
    };
    if (file) {
        reader.readAsDataURL(file); //reads the data as a URL
    }
}

// Multiple modal backgrouund solved

$(document).on('show.bs.modal', '.modal', function (event) {
    var zIndex = 1040 + (10 * $('.modal:visible').length);
    $(this).css('z-index', zIndex);
    setTimeout(function() {
        $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
    }, 0);
});