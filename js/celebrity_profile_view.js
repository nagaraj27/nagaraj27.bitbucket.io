var earning_sheet_duration,video_call_data,advertisements_data,autograph_data,video_msg_data; 
var celebrity_app_use_duration,app_hr_data;
var available_dates=$("#celebrity_availabilty_calendar").data("available-dates");
var element = document.getElementById("availability_calendar");
var myCalendar = jsCalendar.new(element);
myCalendar.reset();
myCalendar.select(available_dates);
$('.celebrity_count,.counter').counterUp({
    delay: 10,
    time: 1000
});
$(document).on('click', '.update_celebrity_base_fare', function () 
    {
        $('#celebrity_base_fare_modal').modal('show');
    });
$(document).on('change', '.celebrity_earing_sheet_filter', function () 
    {
        var filter_val=$(this).val();
    
        if(filter_val=="today")
        {
            earning_sheet_duration = ["Today"]; 
            video_call_data=[49.9];
            advertisements_data=[83.6];
            autograph_data=[48.9];
            video_msg_data=[42.4];
        }
        else if(filter_val=="yesterday")
        {
            earning_sheet_duration = ["Yesterday"]; 
            video_call_data=[106.9];
            advertisements_data=[104.3];
            autograph_data=[65.2];
            video_msg_data=[33.2];
        }
        else if(filter_val=="this_week")
        {
            earning_sheet_duration = ["Mon", "Tue", "Wed","Thr","Fri","Sat","Sun"];
            video_call_data=[176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4];
            advertisements_data=[84.5, 105.0, 104.3, 91.2, 83.5, 106.6, 92.3];
            autograph_data=[45.6, 59.0, 59.6, 52.4, 65.2, 59.3, 51.2];
            video_msg_data=[75.5, 57.4, 60.4, 47.6, 39.1, 46.8, 51.1];
        }
        else if(filter_val=="last_week")
        {
            earning_sheet_duration = ["Mon", "Tue", "Wed","Thr","Fri","Sat","Sun"];
            video_call_data=[49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6];
            advertisements_data=[83.6, 78.8, 98.5, 93.4, 106.0, 84.5, 105.0];
            autograph_data=[48.9, 38.8, 39.3, 41.4, 47.0, 48.3, 59.0, 59.6];
            video_msg_data=[42.4, 33.2, 34.5, 39.7, 52.6, 75.5, 57.4];
        }
        else if(filter_val=="last_6_months")
        {
            earning_sheet_duration = ["Jan", "Feb", "Mar","Apr","May","Jun"];
            video_call_data=[ 129.2, 144.0, 176.0, 135.6, 148.5, 216.4];
            advertisements_data=[93.4, 106.0, 84.5, 105.0, 104.3, 91.2];
            autograph_data=[41.4, 47.0, 48.3, 59.0, 59.6, 52.4];
            video_msg_data=[39.7, 52.6, 75.5, 57.4, 60.4, 47.6];
        }
        else if(filter_val=="last_year")
        {
            earning_sheet_duration = ["Jan", "Feb", "Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
            video_call_data=[49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4];
            advertisements_data=[83.6, 78.8, 98.5, 93.4, 106.0, 84.5, 105.0, 104.3, 91.2, 83.5, 106.6, 92.3];
            autograph_data=[48.9, 38.8, 39.3, 41.4, 47.0, 48.3, 59.0, 59.6, 52.4, 65.2, 59.3, 51.2];
            video_msg_data=[42.4, 33.2, 34.5, 39.7, 52.6, 75.5, 57.4, 60.4, 47.6, 39.1, 46.8, 51.1];
        }
        celebrity_earing_sheet_chart();
    });
    earning_sheet_duration = ["Mon","Tue","Wed","Thr","Fri","Sat","Sun"];
    video_call_data=[144.0, 176.0, 135.6, 148.5, 216.4, 194.1,221];
    advertisements_data=[106.0, 84.5, 105.0, 104.3, 91.2, 83.5,98];
    autograph_data=[47.0, 48.3, 59.0, 59.6, 52.4, 65.2,76];
    video_msg_data=[52.6, 75.5, 57.4, 60.4, 47.6, 39.1,67];
    celebrity_earing_sheet_chart();

    function celebrity_earing_sheet_chart()
{
    Highcharts.chart('celebrity_earing_sheet', {
        chart: {
            type: 'column'
        },
        colors: ['#A4A1FB','#56D9FE','#FF7272','#FFC16D'],
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: earning_sheet_duration,
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Units'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            },
            
        },
        series: [{
            name: 'Video Calls',
            data: video_call_data
    
        }, {
            name: 'Advertisments',
            data: advertisements_data
    
        }, {
            name: 'Autographs',
            data: autograph_data
        }, {
            name: 'Video Messages',
            data: video_msg_data
        }]
    });
}
$(document).on('change', '.celebrity_app_use_filter', function () 
    {
        var filter_val=$(this).val();
        if(filter_val=="today")
        {
           
            celebrity_app_use_duration = ["00:01 - 04:00", "04:00 - 08:00", "08:00 - 12:00","12:00 - 16:00","16:00 - 20:00","20:00 - 23:59"];
            app_hr_data=[176.0, 135.6, 148.5, 216.4, 194.1, 95.6];;
            
        }
        else if(filter_val=="yesterday")
        {
            celebrity_app_use_duration=["00:01 - 04:00", "04:00 - 08:00", "08:00 - 12:00","12:00 - 16:00","16:00 - 20:00","20:00 - 23:59"];;
            app_hr_data=[84.5, 105.0, 104.3, 91.2, 83.5, 106.6];
            
        }
        else if(filter_val=="this_week")
        {
            celebrity_app_use_duration=["Mon", "Tue", "Wed","Thr","Fri","Sat","Sun"];
            app_hr_data=[3,6,8,3,9,4,1];
            
        }
        else if(filter_val=="last_week")
        {
            celebrity_app_use_duration=["Mon", "Tue", "Wed","Thr","Fri","Sat","Sun"];
            app_hr_data=[4,2,4,3,9,4,1];
            
        }
        else if(filter_val=="week_wise")
        {
            celebrity_app_use_duration=["M","T","W","T","F","S","M","T","W","T","F","S","M","T","W","T","F","S","M","T","W","T","F","S","M","T","W","T","F","S"];;
            app_hr_data=[144.0, 176.0, 135.6, 148.5, 216.4, 194.1,221,144.0, 176.0, 135.6, 148.5, 216.4, 194.1,221,144.0, 176.0, 135.6, 148.5, 216.4, 194.1,221,144.0, 176.0, 135.6, 148.5, 216.4, 194.1,221];
            
        }
        else if(filter_val=="last_6_months")
        {
            celebrity_app_use_duration = ["Mon", "Tue", "Wed","Thr","Fri","Sat","Sun"];
            app_hr_data=[14,12,24,13,29,40];
           
        }
        else if(filter_val=="last_year")
        {
            celebrity_app_use_duration = ["Jan", "Feb", "Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
            app_hr_data=[14,12,24,13,29,40,34,5,78,12,23,12];
           
        }
        celebrity_app_use();
    });

celebrity_app_use_duration = ["M","T","W","T","F","S","M","T","W","T","F","S","M","T","W","T","F","S","M","T","W","T","F","S","M","T","W","T","F","S"];
app_hr_data=[144.0, 176.0, 135.6, 148.5, 216.4, 194.1,221,144.0, 176.0, 135.6, 148.5, 216.4, 194.1,221,144.0, 176.0, 135.6, 148.5, 216.4, 194.1,221,144.0, 176.0, 135.6, 148.5, 216.4, 194.1,221];
celebrity_app_use();
function celebrity_app_use()
{
    
Highcharts.chart('celebrity_app_use', {
    chart: {
        type: 'line'
    },
    colors: ['#F83636'],
    title: {
        text: ''
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        categories: celebrity_app_use_duration
    },
    yAxis: {
        title: {
            text: 'Hours'
        }
    },
    plotOptions: {
        line: {
            dataLabels: {
                enabled: false
            },
            enableMouseTracking: true
        }
    },
    series: [{
        name: 'Hours',
        data: app_hr_data
    }]
});
}