var video_msg_data,rev_video_msg_duration;

$(".datatable").DataTable();
$(document).on('change', '.rev_video_msg_filter', function () 
    {
        var filter_val=$(this).val();
        if(filter_val=="today")
        {
            rev_video_msg_duration=["Today"];
            video_msg_data=[5];
            
        }
        else if(filter_val=="yesterday")
        {
            rev_video_msg_duration=["Yesterday"];
            video_msg_data=[3];
            
        }
        else if(filter_val=="this_week")
        {
            rev_video_msg_duration=["Mon", "Tue", "Wed","Thr","Fri","Sat","Sun"];
            video_msg_data=[3,6,8,3,9,4,1];
            
        }
        else if(filter_val=="last_week")
        {
            rev_video_msg_duration=["Mon", "Tue", "Wed","Thr","Fri","Sat","Sun"];
            video_msg_data=[4,2,4,3,9,4,1];
            
        }
        else if(filter_val=="last_6_months")
        {
            rev_video_msg_duration = ["Mon", "Tue", "Wed","Thr","Fri","Sat","Sun"];
            video_msg_data=[14,12,24,13,29,40];
            
        }
        else if(filter_val=="last_year")
        {
            rev_video_msg_duration = ["Jan", "Feb", "Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
            video_msg_data=[14,12,24,13,29,40,34,5,78,12,23,12];
            
        }
        rev_video_msg_chart();
    });
    rev_video_msg_duration = ["Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
    video_msg_data=[40,22,34,13,25,40];
   
    rev_video_msg_chart();



    


    
    

    function rev_video_msg_chart()
    {
        Highcharts.chart('rev_video_msg_chart', {
            chart: {
                type: 'areaspline'
            },
            colors: ['#28DCDF'],
            title: {
                text: ''
            },
            legend: {
                layout: 'vertical',
                align: 'left',
                verticalAlign: 'top',
                x: 150,
                y: 100,
                floating: true,
                borderWidth: 1,
                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
            },
            xAxis: {
                categories: rev_video_msg_duration,
            
            },
            yAxis: {
                title: {
                    text: '₹'
                }
            },
            tooltip: {
                shared: true,
                valueSuffix: ' M'
            },
            credits: {
                enabled: false
            },
            plotOptions: {
                areaspline: {
                    fillOpacity: 0.5
                },
                
            },
            series: [{
                name: 'Video Message',
                data: video_msg_data
            }]
        });
    }
    
    

    