
    var top_rev_duration,video_call_data,advertisements_data,autograph_data,video_msg_data; 
    var revenue_data,payments_data,rev_insights_duration;
    $('.counter').counterUp({
        delay: 10,
        time: 1000
    });
    $(document).on('click', '.update_base_fare', function () 
    {
        $('#business_base_fare_modal').modal('show');
    });
    $(document).on('click', '.update_credit_value', function () 
    {
        $('#update_credit_value_modal').modal('show');
    });
    $(document).on('change', '.revenue_insights_filter', function () 
    {
        var filter_val=$(this).val();
        if(filter_val=="today")
        {
            rev_insights_duration=["Today"];
            revenue_data=[5];
            payments_data=[8];
        }
        else if(filter_val=="yesterday")
        {
            rev_insights_duration=["Yesterday"];
            revenue_data=[3];
            payments_data=[9];
        }
        else if(filter_val=="this_week")
        {
            rev_insights_duration=["Mon", "Tue", "Wed","Thr","Fri","Sat","Sun"];
            revenue_data=[3,6,8,3,9,4,1];
            payments_data=[2, 5, 9, 3, 3, 15];
        }
        else if(filter_val=="last_week")
        {
            rev_insights_duration=["Mon", "Tue", "Wed","Thr","Fri","Sat","Sun"];
            revenue_data=[4,2,4,3,9,4,1];
            payments_data=[20, 15, 19, 39, 4, 5];
        }
        else if(filter_val=="last_6_months")
        {
            rev_insights_duration = ["Mon", "Tue", "Wed","Thr","Fri","Sat","Sun"];
            revenue_data=[14,12,24,13,29,40];
            payments_data=[2, 5, 9, 3, 4,7];
        }
        else if(filter_val=="last_year")
        {
            rev_insights_duration = ["Jan", "Feb", "Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
            revenue_data=[14,12,24,13,29,40,34,5,78,12,23,12];
            payments_data=[2, 5, 9, 3, 4,7,54,23,12,34,87,65];
        }
        revenue_insights_chart();
    });
    rev_insights_duration = ["Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
    revenue_data=[40,22,34,13,25,40];
    payments_data=[25, 50, 29, 38, 34,74];
    revenue_insights_chart();


    $(document).on('change', '.top_revenue_filter', function () 
    {
        var filter_val=$(this).val();
    
        if(filter_val=="today")
        {
            top_rev_duration = ["Today"]; 
            video_call_data=[49.9];
            advertisements_data=[83.6];
            autograph_data=[48.9];
            video_msg_data=[42.4];
        }
        else if(filter_val=="yesterday")
        {
            top_rev_duration = ["Yesterday"]; 
            video_call_data=[106.9];
            advertisements_data=[104.3];
            autograph_data=[65.2];
            video_msg_data=[33.2];
        }
        else if(filter_val=="this_week")
        {
            top_rev_duration = ["Mon", "Tue", "Wed","Thr","Fri","Sat","Sun"];
            video_call_data=[176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4];
            advertisements_data=[84.5, 105.0, 104.3, 91.2, 83.5, 106.6, 92.3];
            autograph_data=[45.6, 59.0, 59.6, 52.4, 65.2, 59.3, 51.2];
            video_msg_data=[75.5, 57.4, 60.4, 47.6, 39.1, 46.8, 51.1];
        }
        else if(filter_val=="last_week")
        {
            top_rev_duration = ["Mon", "Tue", "Wed","Thr","Fri","Sat","Sun"];
            video_call_data=[49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6];
            advertisements_data=[83.6, 78.8, 98.5, 93.4, 106.0, 84.5, 105.0];
            autograph_data=[48.9, 38.8, 39.3, 41.4, 47.0, 48.3, 59.0, 59.6];
            video_msg_data=[42.4, 33.2, 34.5, 39.7, 52.6, 75.5, 57.4];
        }
        else if(filter_val=="last_6_months")
        {
            top_rev_duration = ["Jan", "Feb", "Mar","Apr","May","Jun"];
            video_call_data=[ 129.2, 144.0, 176.0, 135.6, 148.5, 216.4];
            advertisements_data=[93.4, 106.0, 84.5, 105.0, 104.3, 91.2];
            autograph_data=[41.4, 47.0, 48.3, 59.0, 59.6, 52.4];
            video_msg_data=[39.7, 52.6, 75.5, 57.4, 60.4, 47.6];
        }
        else if(filter_val=="last_year")
        {
            top_rev_duration = ["Jan", "Feb", "Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
            video_call_data=[49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4];
            advertisements_data=[83.6, 78.8, 98.5, 93.4, 106.0, 84.5, 105.0, 104.3, 91.2, 83.5, 106.6, 92.3];
            autograph_data=[48.9, 38.8, 39.3, 41.4, 47.0, 48.3, 59.0, 59.6, 52.4, 65.2, 59.3, 51.2];
            video_msg_data=[42.4, 33.2, 34.5, 39.7, 52.6, 75.5, 57.4, 60.4, 47.6, 39.1, 46.8, 51.1];
        }
        top_revenue_chart();
    });

    top_rev_duration = ["Mon","Tue","Wed","Thr","Fri","Sat","Sun"];
    video_call_data=[144.0, 176.0, 135.6, 148.5, 216.4, 194.1,221];
    advertisements_data=[106.0, 84.5, 105.0, 104.3, 91.2, 83.5,98];
    autograph_data=[47.0, 48.3, 59.0, 59.6, 52.4, 65.2,76];
    video_msg_data=[52.6, 75.5, 57.4, 60.4, 47.6, 39.1,67];
    top_revenue_chart();


/* Revenue Insights Charts */
function revenue_insights_chart()
{
    Highcharts.chart('rev_insights', {
        chart: {
            type: 'areaspline'
        },
        colors: ['#A3A0FB','#54D8FF'],
        title: {
            text: ''
        },
        legend: {
            layout: 'vertical',
            align: 'left',
            verticalAlign: 'top',
            x: 150,
            y: 100,
            floating: true,
            borderWidth: 1,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
        },
        xAxis: {
            categories: rev_insights_duration,
           
        },
        yAxis: {
            title: {
                text: 'Units'
            }
        },
        tooltip: {
            shared: true,
            valueSuffix: ' units'
        },
        credits: {
            enabled: false
        },
        plotOptions: {
            areaspline: {
                fillOpacity: 0.5
            },
            
        },
        series: [{
            name: 'Revenue',
            data: revenue_data
        }, {
            name: 'Payments',
            data: payments_data
        }]
    });
}



/* Top Revenue */
function top_revenue_chart()
{
    Highcharts.chart('top_revenue', {
        chart: {
            type: 'column'
        },
        colors: ['#A4A1FB','#56D9FE','#FF7272','#FFC16D'],
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: top_rev_duration,
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Units'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            },
            
        },
        series: [{
            name: 'Video Calls',
            data: video_call_data
    
        }, {
            name: 'Advertisments',
            data: advertisements_data
    
        }, {
            name: 'Autographs',
            data: autograph_data
        }, {
            name: 'Video Messages',
            data: video_msg_data
        }]
    });
}

