var standard_data,personalised_data,rev_autograph_duration;
$(".datatable").DataTable();
$(document).on('change', '.rev_autograph_filter', function () 
{
    var filter_val=$(this).val();
    if(filter_val=="today")
    {
        rev_autograph_duration=["Today"];
        standard_data=[5];
        personalised_data=[8];
    }
    else if(filter_val=="yesterday")
    {
        rev_autograph_duration=["Yesterday"];
        standard_data=[3];
        personalised_data=[9];
    }
    else if(filter_val=="this_week")
    {
        rev_autograph_duration=["Mon", "Tue", "Wed","Thr","Fri","Sat","Sun"];
        standard_data=[3,6,8,3,9,4,1];
        personalised_data=[2, 5, 9, 3, 3, 15];
    }
    else if(filter_val=="last_week")
    {
        rev_autograph_duration=["Mon", "Tue", "Wed","Thr","Fri","Sat","Sun"];
        standard_data=[4,2,4,3,9,4,1];
        personalised_data=[20, 15, 19, 39, 4, 5];
    }
    else if(filter_val=="last_6_months")
    {
        rev_autograph_duration = ["Mon", "Tue", "Wed","Thr","Fri","Sat","Sun"];
        standard_data=[14,12,24,13,29,40];
        personalised_data=[2, 5, 9, 3, 4,7];
    }
    else if(filter_val=="last_year")
    {
        rev_autograph_duration = ["Jan", "Feb", "Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
        standard_data=[14,12,24,13,29,40,34,5,78,12,23,12];
        personalised_data=[2, 5, 9, 3, 4,7,54,23,12,34,87,65];
    }
    total_revenue_chart();
});
rev_autograph_duration = ["Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
standard_data=[40,22,34,13,25,40];
personalised_data=[25, 50, 29, 38, 34,74];
total_revenue_chart();

function total_revenue_chart()
{
    Highcharts.chart('rev_autograph_chart', {
        chart: {
            type: 'areaspline'
        },
        colors: ['#A3A0FB','#CF2150'],
        title: {
            text: ''
        },
        legend: {
            layout: 'vertical',
            align: 'left',
            verticalAlign: 'top',
            x: 150,
            y: 100,
            floating: true,
            borderWidth: 1,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
        },
        xAxis: {
            categories: rev_autograph_duration,
        
        },
        yAxis: {
            title: {
                text: '₹'
            }
        },
        tooltip: {
            shared: true,
            valueSuffix: ' M'
        },
        credits: {
            enabled: false
        },
        plotOptions: {
            areaspline: {
                fillOpacity: 0.5
            },
            
        },
        series: [{
            name: 'Standard',
            data: standard_data
        }, {
            name: 'Personalised',
            data: personalised_data
        }]
    });
}