var peak_hours_duration,hour_data; 
peak_hours_duration = ["00:01 - 04:00", "04:00 - 08:00", "08:00 - 12:00","12:00 - 16:00","16:00 - 20:00","20:00 - 23:59"];
$('.fans_count,.current_wallet_bal').counterUp({
    delay: 10,
    time: 1000
});
$(document).on('change', '.user_app_filter', function () 
    {
        var filter_val=$(this).val();
    
        if(filter_val=="today")
        {
           
            hour_data=[176.0, 135.6, 148.5, 216.4, 194.1, 95.6];
        }
        else if(filter_val=="yesterday")
        {
            hour_data=[84.5, 105.0, 104.3, 91.2, 83.5, 106.6];
        }
        else if(filter_val=="this_week")
        {
            hour_data=[45.6, 59.0, 59.6, 52.4, 65.2, 59.3];
            
        }
        else if(filter_val=="last_week")
        {
            hour_data=[48.9, 38.8, 39.3, 41.4, 47.0, 48.3, 59.0];
        }
        else if(filter_val=="last_6_months")
        {
            hour_data=[49.9, 71.5, 106.4, 129.2, 144.0, 176.0];
        }
        else if(filter_val=="last_year")
        {
            hour_data=[129.2, 144.0, 176.0, 135.6, 148.5];
        }
        user_app_hours_chart    ();
    });
hour_data=[144.0, 176.0, 135.6, 148.5, 216.4, 194.1];
user_app_hours_chart();
      function user_app_hours_chart()
      {
          Highcharts.chart('user_app_hours_chart', {
              chart: {
                  type: 'column'
              },
              colors: ['#56D9FE'],
              title: {
                  text: ''
              },
              subtitle: {
                  text: ''
              },
              xAxis: {
                  categories: peak_hours_duration,
                  crosshair: true
              },
              yAxis: {
                  min: 0,
                  title: {
                      text: 'Hours'
                  }
              },
              tooltip: {
                  headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                  pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                      '<td style="padding:0"><b>{point.y:.1f}h</b></td></tr>',
                  footerFormat: '</table>',
                  shared: true,
                  useHTML: true
              },
              
              plotOptions: {
                  column: {
                      pointPadding: 0.2,
                      borderWidth: 0,
                      dataLabels: {
                        enabled: true,
                        color: '#56D9FE',
                        
                        format: '{y} h',
                      
                        style: {
                            fontWeight: 'bold'
                        },
                        
                    },
                  },
                  
              },
              series: [{
                  name: 'Peak Hours',
                  data: hour_data
          
              }]
          });
      }